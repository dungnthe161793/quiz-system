﻿using System;
using System.Collections.Generic;

namespace PRN211_Project.Models
{
    public partial class Review
    {
        public Review()
        {
            InverseParentReview = new HashSet<Review>();
        }

        public int ReviewId { get; set; }
        public int? UserId { get; set; }
        public int? QuizId { get; set; }
        public string? Comment { get; set; }
        public DateTime? ReviewDate { get; set; }
        public int? ParentReviewId { get; set; }

        public virtual Review? ParentReview { get; set; }
        public virtual Quiz? Quiz { get; set; }
        public virtual User? User { get; set; }
        public virtual ICollection<Review> InverseParentReview { get; set; }
    }
}
