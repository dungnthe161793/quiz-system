﻿using System;
using System.Collections.Generic;

namespace PRN211_Project.Models
{
    public partial class Question
    {
        public Question()
        {
            Choices = new HashSet<Choice>();
        }

        public int QuestionId { get; set; }
        public int? QuizId { get; set; }
        public string? Type { get; set; }
        public string? Content { get; set; }
        public int Point { get; set; }

        public virtual Quiz? Quiz { get; set; }
        public virtual ICollection<Choice> Choices { get; set; }
    }
}
