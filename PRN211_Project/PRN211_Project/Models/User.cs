﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PRN211_Project.Models
{
    public partial class User
    {
        public User()
        {
            DoQuizzes = new HashSet<DoQuiz>();
            Reviews = new HashSet<Review>();
            UserRoles = new HashSet<UserRole>();
        }

        public int UserId { get; set; }
        [Required(ErrorMessage = "User name must not be blank!")]
        public string Username { get; set; } = null!;
        [Required(ErrorMessage = "Invalid email!")]
        public string Email { get; set; } = null!;
        [StringLength(225, MinimumLength = 6)]
        [RegularExpression(@"^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{5,}$", ErrorMessage = "String must contain at least one letter and one number.")]
        [Required(ErrorMessage = "Password is not null!")]
        public string Password { get; set; } = null!;
        public string? Avatar { get; set; }
        //[RegularExpression(@"^(0|\+84)[1-9]\d{9}$", ErrorMessage = "Invalid phone number")]
        [Required(ErrorMessage = "Phone number is not null!")]
        public string Phone { get; set; }
        public string? Address { get; set; }

        public virtual ICollection<DoQuiz> DoQuizzes { get; set; }
        public virtual ICollection<Review> Reviews { get; set; }
        public virtual ICollection<UserRole> UserRoles { get; set; }
    }
}
