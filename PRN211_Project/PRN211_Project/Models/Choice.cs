﻿using System;
using System.Collections.Generic;

namespace PRN211_Project.Models
{
    public partial class Choice
    {
        public int ChoiceId { get; set; }
        public int? QuestionId { get; set; }
        public string? Content { get; set; }
        public bool IsCorrect { get; set; }

        public virtual Question? Question { get; set; }
    }
}
