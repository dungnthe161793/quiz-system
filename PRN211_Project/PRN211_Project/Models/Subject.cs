﻿using System;
using System.Collections.Generic;

namespace PRN211_Project.Models
{
    public partial class Subject
    {
        public Subject()
        {
            Quizzes = new HashSet<Quiz>();
        }

        public int SubjectId { get; set; }
        public string SubjectName { get; set; } = null!;

        public virtual ICollection<Quiz> Quizzes { get; set; }
    }
}
