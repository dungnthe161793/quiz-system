﻿using System;
using System.Collections.Generic;

namespace PRN211_Project.Models
{
    public partial class DoQuiz
    {
        public int DoQuizzesId { get; set; }
        public int? UserId { get; set; }
        public int? QuizId { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public int? Score { get; set; }

        public virtual Quiz? Quiz { get; set; }
        public virtual User? User { get; set; }
    }
}
