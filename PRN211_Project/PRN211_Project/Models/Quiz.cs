﻿using System;
using System.Collections.Generic;

namespace PRN211_Project.Models
{
    public partial class Quiz
    {
        public Quiz()
        {
            DoQuizzes = new HashSet<DoQuiz>();
            Questions = new HashSet<Question>();
            Reviews = new HashSet<Review>();
        }

        public int QuizId { get; set; }
        public string Title { get; set; } = null!;
        public TimeSpan? Time { get; set; }
        public int? SubjectId { get; set; }

        public virtual Subject? Subject { get; set; }
        public virtual ICollection<DoQuiz> DoQuizzes { get; set; }
        public virtual ICollection<Question> Questions { get; set; }
        public virtual ICollection<Review> Reviews { get; set; }
    }
}
