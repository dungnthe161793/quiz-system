﻿using System.Text;

namespace PRN211_Project.Utility
{
    public class Utilities
    {
        private static Random random = new Random();
        public static string Generate(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            StringBuilder result = new StringBuilder(length);
            for (int i = 0; i < length; i++)
            {
                result.Append(chars[random.Next(chars.Length)]);
            }
            return result.ToString();
        }

        public static string SaveImage(IFormFile image)
        {
            string filePath = Path.Combine("./wwwroot/avatars/", image.FileName);
            using (Stream fileStream = new FileStream(filePath, FileMode.Create, FileAccess.Write))
            {
                image.CopyTo(fileStream);
            }

            return image.FileName;
        }
    }
}
