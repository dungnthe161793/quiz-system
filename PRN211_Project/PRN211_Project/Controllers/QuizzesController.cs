﻿using PRN211_Project.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Org.BouncyCastle.Asn1.Ess;

namespace Demo.Controllers
{
    public class QuizzesController : Controller
    {

        private readonly PRN211_BL5DBContext db;
        public QuizzesController(PRN211_BL5DBContext db)
        { 
            this.db = db; 
        }

        public async Task<IActionResult> Index()
        {
            var quizzes = await db.Quizzes
            .Include(q => q.Subject)
            .ToListAsync();
            return View(quizzes);
        }

        public IActionResult Detail(int id)
        {
            var quiz = db.Quizzes
                .Include(q => q.Subject) // Include để load thông tin môn học liên quan
                .FirstOrDefault(q => q.QuizId == id);

            if (quiz == null)
            {
                return NotFound();
            }

            return View(quiz);
        }

        public IActionResult Create()
        {

            // Lấy danh sách môn học từ database
            var subjects = db.Subjects.ToList();

            // Chuyển danh sách môn học thành SelectList để truyền vào View
            ViewBag.Subjects = new SelectList(subjects, "SubjectId", "SubjectName");

            return View();
        }


        [HttpPost]
        public IActionResult Create(Quiz quiz)
        {
            if (ModelState.IsValid)
            {
               
                // Lấy subject từ database dựa vào SubjectId được chọn
                var selectedSubject = db.Subjects.Find(quiz.SubjectId);
                if (selectedSubject == null)
                {
                    ModelState.AddModelError("SubjectId", "Invalid subject selection.");
                    return View(quiz);
                }

                Quiz newQuiz = new Quiz();
                newQuiz.Title = quiz.Title;
                newQuiz.Time = quiz.Time;
                newQuiz.Subject = selectedSubject; // Gán subject đã chọn vào đề thi
                db.Quizzes.Add(newQuiz);
                db.SaveChanges();
                return RedirectToAction("AddQuestion", "Question");
            }

            // Nếu ModelState không hợp lệ, trả về View với thông tin quiz và danh sách môn học
            ViewBag.Subjects = new SelectList(db.Subjects.ToList(), "SubjectId", "SubjectName");
            return View(quiz);
        }

        public IActionResult Edit(int id)
        {
            var quiz = db.Quizzes.Find(id);
            if (quiz == null)
            {
                return NotFound();
            }

            return View(quiz);
        }

        [HttpPost]
        public IActionResult Edit(Quiz quiz)
        {
            if (ModelState.IsValid)
            {
                db.Quizzes.Update(quiz);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(quiz);
        }

        public IActionResult Delete(int id)
        {
            var quiz = db.Quizzes.Find(id);
            if (quiz == null)
            {
                return NotFound();
            }

            return View(quiz);
        }

        public IActionResult DeleteConfirmed(int id)
        {
            Quiz quiz = db.Quizzes.Find(id);
            if (quiz == null)
            {
                return NotFound();
            }
            var listquestion = db.Questions.Where(q => q.QuizId == id).ToList();
            foreach(Question question in listquestion)
            {
                var listchoice = db.Choices.Where(c => c.QuestionId == question.QuestionId).ToList();
                foreach(Choice choice in listchoice)
                {
                    db.Choices.Remove(choice);
                    db.SaveChanges();
                }
                db.Questions.Remove(question);
                db.SaveChanges();
            }
            db.Quizzes.Remove(quiz);
            db.SaveChanges();
            
            return RedirectToAction("Index");
        }

        public IActionResult CreateSubject()
        {
            var subjects = db.Subjects.ToList();
            ViewBag.ExistingSubjects = subjects;
            return View();
        }

        [HttpPost]
        public IActionResult CreateSubject(string newSubjectName)
        {
            if (!string.IsNullOrWhiteSpace(newSubjectName))
            {
                var newSubject = new Subject { SubjectName = newSubjectName };
                db.Subjects.Add(newSubject);
                db.SaveChanges();

                TempData["SuccessMessage"] = "Subject created successfully.";
            }

            return RedirectToAction("CreateSubject");
        }

        public IActionResult EditSubject(int id)
        {
            var subject = db.Subjects.Find(id);
            if (subject == null)
            {
                return NotFound();
            }

            return View(subject);
        }

        [HttpPost]
        public IActionResult EditSubject(Subject subject)
        {
            if (ModelState.IsValid)
            {
                db.Subjects.Update(subject);
                db.SaveChanges();
                return RedirectToAction("CreateSubject");
            }

            return View(subject);
        }
    }
}
