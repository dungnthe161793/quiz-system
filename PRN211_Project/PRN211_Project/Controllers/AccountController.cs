﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PRN211_Project.Models;
using PRN211_Project.Utility;
using System.Security.Principal;

namespace PRN211_Project.Controllers
{
    public class AccountController : Controller
    {
        PRN211_BL5DBContext db = new PRN211_BL5DBContext();
        private readonly ISendMailService _sendMailService;
        public AccountController(ISendMailService sendMailService)
        {
            _sendMailService = sendMailService;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> Information()
        {
            var id = HttpContext.Session.GetInt32("UserId");
            User temp = await db.Users.FirstOrDefaultAsync(x => x.UserId == id);
            return View(temp);
        }

        [HttpGet]
        public async Task<IActionResult> forgot_password()
        {
            return View("Password");
        }

        [HttpGet]
        public async Task<IActionResult> change_password()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> change_password(string pass, string repass)
        {
            if (string.IsNullOrEmpty(pass) || string.IsNullOrEmpty(repass))
            {
                TempData["errorpass"] = "All inputs must not be null!";
                return View();
            }
            if (!pass.Equals(repass))
            {
                TempData["errorpass"] = "Re-password is invalid!";
                return View();
            }
            var id = HttpContext.Session.GetString("tempuser");
            var user = await db.Users.FirstOrDefaultAsync(x => x.UserId == int.Parse(id));
            user.Password = repass;
            db.SaveChanges();
            return RedirectToAction("Login", "Home");
        }

        [HttpGet]
        public async Task<IActionResult> user_change_password()
        {
            if (HttpContext.Session.GetInt32("UserRole") == null)
            {
                return RedirectToAction("HomePage", "Home");
            }
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> user_change_password(string oldpass, string pass, string repass)
        {
            if (HttpContext.Session.GetInt32("UserRole") == null)
            {
                return RedirectToAction("HomePage", "Home");
            }
            var id = HttpContext.Session.GetInt32("UserID");
            if (id == null)
            {
                return RedirectToAction("HomePage", "Home");
            }
            var temp = await db.Users.FirstOrDefaultAsync(x => x.UserId == id);

            Console.WriteLine("change pass 2");
            if (!string.IsNullOrEmpty(oldpass))
            {
                if (!oldpass.Equals(temp.Password))
                {
                    TempData["errorpass"] = "Old password is not correct! Try again";
                    return View();
                }
            }
            if (string.IsNullOrEmpty(pass) || string.IsNullOrEmpty(repass))
            {
                TempData["errorpass"] = "All inputs must not be null!";
                return View();
            }
            if (!pass.Equals(repass))
            {
                TempData["errorpass"] = "Re-password is invalid!";
                return View();
            }
            temp.Password = repass;
            await db.SaveChangesAsync();
            return RedirectToAction("Login", "Home");
        }

        [HttpPost]
        public async Task<IActionResult> SendMail(string info)
        {
            User temp;
            if (string.IsNullOrEmpty(info))
            {
                return Json(new { success = false, message = "Enter your email or username" });
            }
            if (info.Contains('@'))
            {
                temp = await db.Users.FirstOrDefaultAsync(x => x.Email.Equals(info));
            }
            else
            {
                temp = await db.Users.FirstOrDefaultAsync(x => x.Username.Equals(info));
            }
            if (temp == null)
            {
                return Json(new { success = false, message = "This account hasn't been registered!" });
            }

            var code = Utilities.Generate(10);
            MailContent content = new MailContent
            {
                To = temp.Email,
                Subject = "Kiểm tra thử",
                Body = $"<p><strong>Heloo, {temp.Username}, this is your code to change your password: {code}</strong></p>"
            };

            await _sendMailService.SendMail(content);
            HttpContext.Session.SetString("tempuser", temp.UserId.ToString());
            //Console.WriteLine("Send mail success!");
            return Json(new { success = true, message = "An email has been sent to you. Please check to get the code!", code = code });
        }

        [HttpPost]
        public async Task<IActionResult> CheckEmail(string email)
        {
            if (string.IsNullOrEmpty(email))
            {
                return Json(new { success = false, message = "Please input!" });
            }
            if (email.Contains("@"))
            {
                var temp = await db.Users.FirstOrDefaultAsync(x => x.Email.Equals(email));
                if (temp != null)
                {
                    return Json(new { success = false, message = "This has been used!" });
                }
            }
            else
            {
                var temp = await db.Users.FirstOrDefaultAsync(x => x.Username.Equals(email));
                if (temp != null)
                {
                    return Json(new { success = false, message = "This has been used!" });
                }
            }

            return Json(new { success = true });
        }

        [HttpPost]
        public async Task<IActionResult> Update(string name, string email, string phone, string address, IFormFile filename)
        {
            if (HttpContext.Session.GetInt32("UserRole") == null)
            {
                return RedirectToAction("HomePage", "Home");
            }
            User u = new User()
            {
                Username = name,
                Phone = phone,
                Address = address,
                Email = email,
            };

            u.Avatar = "default.jpg";

            var id = HttpContext.Session.GetInt32("UserId");
            var temp = await db.Users.FirstOrDefaultAsync(x => x.UserId == id);
            u.Avatar = temp.Avatar;
            temp.Username = u.Username;

            if (filename != null)
            {
                if (filename.FileName.Length > 0)
                {
                    u.Avatar = Utilities.SaveImage(filename);
                }
            }
            temp.Avatar = u.Avatar;
            temp.Email = u.Email;
            temp.Phone = u.Phone;
            temp.Address = u.Address;
            await db.SaveChangesAsync();
            HttpContext.Session.Remove("avatar");
            HttpContext.Session.SetString("avatar", temp.Avatar);
            return RedirectToAction("Information");
        }

        [HttpGet]
        public async Task<IActionResult> Manager()
        {
            if (HttpContext.Session.GetInt32("UserRole") == null || HttpContext.Session.GetInt32("UserRole") != 1)
            {
                return RedirectToAction("HomePage", "Home");
            }

            var cid = Request.Query["cid"];

            if (string.IsNullOrEmpty(cid))
            {
                var list = await db.UserRoles.Include(x => x.User).ToListAsync();
                ViewBag.roles = await db.Roles.ToListAsync();
                ViewBag.lists = list;
                if (list == null)
                {
                    TempData["accountmsg"] = "There is no account!";
                }
            }
            else
            {
                var list = await db.UserRoles.Include(x => x.User).Where(x => x.RoleId == int.Parse(cid)).ToListAsync();
                ViewBag.roles = await db.Roles.ToListAsync();
                ViewBag.lists = list;
                if (list == null)
                {
                    TempData["accountmsg"] = "There is no account!";
                }
            }
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> Ban(string id)
        {
            var temp = await db.UserRoles.Include(x => x.User).FirstOrDefaultAsync(x => x.User.UserId == int.Parse(id));
            temp.IsActive = temp.IsActive == byte.Parse("1") ? byte.Parse("0") : byte.Parse("1");
            Console.WriteLine(temp.IsActive);
            db.SaveChangesAsync();
            return RedirectToAction("Manager");
        }

        [HttpPost]
        public async Task<IActionResult> CheckName(string name)
        {
            var temp = await db.Users.FirstOrDefaultAsync(x => x.Username.Equals(name));

            if (temp != null)
            {
                Console.WriteLine("0");
                if (HttpContext.Session.GetInt32("UserId") != null)
                {
                    if (temp.UserId == HttpContext.Session.GetInt32("UserId"))
                    {
                        Console.WriteLine("1");
                        return Json(new { success = true });
                    }
                    else
                    {
                        Console.WriteLine("1.5");
                        return Json(new { success = false, message = "This name has been used!" });
                    }
                }
                Console.WriteLine("2");
                return Json(new { success = false, message = "This name has been used!" });
            }
            Console.WriteLine("3");
            return Json(new { success = true });
        }
        [HttpGet]
        public async Task<IActionResult> CreateAdmin()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CreateAdmin(User u)
        {
            if (!ModelState.IsValid)
            {
                return View("CreateAdmin");
            }
            //Console.WriteLine("User:" + u.UserId);
            db.Users.Add(u);
            db.SaveChanges();
            var temp1 = await db.Users.OrderBy(x => x.UserId).LastAsync();
            Console.WriteLine(temp1.UserId);
            //u.UserId = temp1.UserId + 1;
            db.UserRoles.Add(new UserRole()
            {
                UserId = temp1.UserId,
                RoleId = 1,
                IsActive = 1
            });
            db.SaveChanges();
            return RedirectToAction("Manager");
        }
    }
}
