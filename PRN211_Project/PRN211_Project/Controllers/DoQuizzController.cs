﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using PRN211_Project.Models;
using System.Drawing;

namespace PRN211_Project.Controllers
{
    public class DoQuizzController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult AllHistory()
        {
            if (HttpContext.Session.GetInt32("UserRole") == null)
            {
                return RedirectToAction("HomePage", "Home");
            }
            PRN211_BL5DBContext db = new PRN211_BL5DBContext();
            var DoQuizzes = db.DoQuizzes.Include(x => x.Quiz).Include(x => x.User).OrderByDescending(d => d.DoQuizzesId).ToList();
            ViewBag.DoQuizzes = DoQuizzes;
            return View();
        }

        public IActionResult HistoryByQuizz(int quizid)
        {
            if (HttpContext.Session.GetInt32("UserRole") == null)
            {
                return RedirectToAction("HomePage", "Home");
            }
            PRN211_BL5DBContext db = new PRN211_BL5DBContext();
            var DoQuizzes = db.DoQuizzes.Include(x => x.Quiz).Include(x => x.User).Where(x => x.QuizId == quizid).OrderByDescending(d => d.DoQuizzesId).ToList();
            ViewBag.DoQuizzes = DoQuizzes;
            return View();
        }

        public IActionResult HistoryByUser(int userid)
        {
            if (HttpContext.Session.GetInt32("UserRole") == null)
            {
                return RedirectToAction("HomePage", "Home");
            }
            PRN211_BL5DBContext db = new PRN211_BL5DBContext();
            var DoQuizzes = db.DoQuizzes.Include(x => x.Quiz).Include(x => x.User).Where(x => x.UserId == userid).OrderByDescending(d => d.DoQuizzesId).ToList();
            ViewBag.DoQuizzes = DoQuizzes;
            return View();
        }

        static void Shuffle<T>(List<T> list)
        {
            Random random = new Random();

            for (int i = list.Count - 1; i > 0; i--)
            {
                int j = random.Next(i + 1);
                T temp = list[i];
                list[i] = list[j];
                list[j] = temp;
            }
        }

        public IActionResult DoQuiz(int quizid, int questionid, int answer)
        {

            PRN211_BL5DBContext db = new PRN211_BL5DBContext();
            int maxQuestion = db.Questions.Where(q => q.QuizId == quizid).Max(q => q.QuestionId);
            //    ViewBag.countdown = DateTime.Now + db.Quizzes.Find(quizid).Time;
            if (questionid > maxQuestion)
            {
                return RedirectToAction("Result", quizid);
            }
            if ((questionid % 5) == 1)
            {
                var a = DateTime.Now + db.Quizzes.Find(quizid).Time;
                HttpContext.Session.SetString("time", ((DateTime.Now + db.Quizzes.Find(quizid).Time).ToString()));
                HttpContext.Session.SetString("start", (DateTime.Now).ToString());

                HttpContext.Session.SetInt32("point", 0);
            }

            DateTime sessionTime = DateTime.Parse(HttpContext.Session.GetString("time"));
            ViewBag.time = DateTime.Now + db.Quizzes.Find(quizid).Time;
            //      ViewBag.countdown = sessionTime - DateTime.Now;
            var questions = db.Questions.Where(q => q.QuestionId == questionid).ToList();
            List<Choice> choices = db.Choices.Where(q => q.QuestionId == questionid).ToList();
            Shuffle(choices);

            ViewBag.questions = questions;
            ViewBag.choices = choices;
            ViewBag.quizid = quizid;
            ViewBag.questionid = questionid + 1;
            ViewBag.start = DateTime.Now;
            Choice choice = db.Choices.Find(answer);
            if (choice != null)
            {
                if (choice.IsCorrect == true)
                {
                    int currentIntValue = HttpContext.Session.GetInt32("point") ?? 0;
                    HttpContext.Session.SetInt32("point", currentIntValue + db.Questions.Find(questionid).Point);
                }
            }
            return View();
        }

        public IActionResult DoQuizz(int quizid, int questionid, int answer)
        {
            if (HttpContext.Session.GetInt32("UserRole") == null)
            {
                return RedirectToAction("HomePage", "Home");
            }
            PRN211_BL5DBContext db = new PRN211_BL5DBContext();
            int maxQuestion = db.Questions.Where(q => q.QuizId == quizid).Max(q => q.QuestionId);
            //    ViewBag.countdown = DateTime.Now + db.Quizzes.Find(quizid).Time;
            if (questionid > maxQuestion)
            {
                return RedirectToAction("Result", new { quizid = quizid });
            }
            if (questionid == db.Questions.Where(q => q.QuizId == quizid).Min(q => q.QuestionId))
            {
                var a = DateTime.Now + db.Quizzes.Find(quizid).Time;
                HttpContext.Session.SetString("time", ((DateTime.Now + db.Quizzes.Find(quizid).Time).ToString()));
                HttpContext.Session.SetString("start", (DateTime.Now).ToString());

                HttpContext.Session.SetInt32("point", 0);
            }

            DateTime sessionTime = DateTime.Parse(HttpContext.Session.GetString("time"));
            ViewBag.time = sessionTime;
            //      ViewBag.countdown = sessionTime - DateTime.Now;
            var questions = db.Questions.Where(q => q.QuestionId == questionid).ToList();
            List<Choice> choices = db.Choices.Where(q => q.QuestionId == questionid).ToList();
            Shuffle(choices);

            ViewBag.questions = questions;
            ViewBag.choices = choices;
            ViewBag.quizid = quizid;
            ViewBag.questionid = questionid + 1;
            ViewBag.start = DateTime.Now;
            Choice choice = db.Choices.Find(answer);
            if (choice != null)
            {
                if (choice.IsCorrect == true)
                {
                    int currentIntValue = HttpContext.Session.GetInt32("point") ?? 0;
                    HttpContext.Session.SetInt32("point", currentIntValue + db.Questions.Find(questionid).Point);
                }
            }
            return View();
        }

        public IActionResult AllQuiz()
        {
            if (HttpContext.Session.GetInt32("UserRole") == null)
            {
                return RedirectToAction("HomePage", "Home");
            }
            PRN211_BL5DBContext db = new PRN211_BL5DBContext();
            var quiz = db.Quizzes.Include(q => q.Subject).ToList();
            ViewBag.quiz = quiz;
            return View();
        }

        public IActionResult Quiz(int quizid)
        {
            if (HttpContext.Session.GetInt32("UserRole") == null)
            {
                return RedirectToAction("HomePage", "Home");
            }
            PRN211_BL5DBContext db = new PRN211_BL5DBContext();
            HttpContext.Session.SetString("time", DateTime.Now.ToString());
            Quiz quiz = db.Quizzes.Find(quizid);
            ViewBag.subject = db.Subjects.Find(quiz.SubjectId).SubjectName;
            int questionid = db.Questions.Where(q => q.QuizId == quizid).Min(q => q.QuestionId);
            ViewBag.questionid = questionid;
            ViewBag.quizid = quizid;

            return View(quiz);
        }




        public IActionResult Result(int quizid)
        {
            PRN211_BL5DBContext db = new PRN211_BL5DBContext();
            DateTime start = DateTime.Parse(HttpContext.Session.GetString("start"));
            ViewBag.start = start;
            ViewBag.end = (DateTime.Now).ToString("dddd,d MMM yyyy,HH:mm tt");
            TimeSpan timeDifference = DateTime.Now - DateTime.Parse(HttpContext.Session.GetString("start"));
            ViewBag.time = timeDifference.ToString("hh':'mm':'ss");
            var id = HttpContext.Session.GetInt32("UserId");
            ViewBag.name = db.Users.Find(id).Username;
            ViewBag.point = HttpContext.Session.GetInt32("point");
            DoQuiz doQuiz = new DoQuiz();
            doQuiz.UserId = id;
            doQuiz.QuizId = quizid;
            doQuiz.StartTime = start;
            doQuiz.EndTime = DateTime.Now;
            doQuiz.Score = HttpContext.Session.GetInt32("point");
            Console.WriteLine("userid:" + doQuiz.UserId);
            Console.WriteLine(quizid);
            Console.WriteLine("quizid: " + doQuiz.QuizId);
            db.Add(doQuiz);
            db.SaveChanges();
            return View();
        }
    }
}
