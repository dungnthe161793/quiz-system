﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PRN211_Project.Models;
using System.Data.SqlTypes;
using System.IO;
using System.Net.WebSockets;

namespace PRNBL5.Controllers
{
    public class QuestionController : Controller
    {
        //private readonly PRN211_BL5DBContext db;

        //public QuestionController(PRN211_BL5DBContext db)
        //{
        //    this.db = db;
        //}
        public async Task<IActionResult> List()
        {
            PRN211_BL5DBContext db=new PRN211_BL5DBContext();
            var e = await db.Questions.Include(e => e.Quiz).Include(e => e.Choices).ToListAsync();

            return View(e);

            //var questions = await db.Questions.Include(e => e.Choices).ToListAsync();
            //return View(questions);

        }
        public async Task<IActionResult> DetailQuestion(int id)
        {
            PRN211_BL5DBContext db = new PRN211_BL5DBContext();

            Question q = new Question();
            q = db.Questions.FirstOrDefault(q => q.QuestionId == id);
            List<Choice> choices = new List<Choice>();
            choices = db.Choices.Where(c => c.QuestionId == id).ToList();

            ViewBag.Choice = choices;


            return View(q);
        }
        [HttpPost]
        public async Task<IActionResult> EditProcess(Question q, List<Choice> choices, int correctAnswerId)

        {
            PRN211_BL5DBContext db = new PRN211_BL5DBContext();

            // Find the question by its ID
            var question = await db.Questions.FindAsync(q.QuestionId);

            if (question != null)
            {
                // Update the question content
                question.Content = q.Content;
                bool correctAnswerFound = false;
                // Update choices
                foreach (var updatedChoice in choices)
                {
                    var choice = await db.Choices.FindAsync(updatedChoice.ChoiceId);
                    if (choice != null)
                    {
                        choice.Content = updatedChoice.Content;

                        choice.IsCorrect = choice.ChoiceId == correctAnswerId;


                    }
                }
                await db.SaveChangesAsync();
            }
            return RedirectToAction("List");
        }


        [HttpGet]

        public async Task<IActionResult> AddQuestion()
        {
            PRN211_BL5DBContext db = new PRN211_BL5DBContext();

            var quizzes = await db.Quizzes.ToListAsync(); // Fetch quizzes from your database
            ViewBag.Quizzes = new SelectList(quizzes, "QuizId", "Title");
            return View();
        }

        [HttpPost]

        public async Task<IActionResult> AddQuestion(Question question, string answer, string[] c)
        {
            PRN211_BL5DBContext db = new PRN211_BL5DBContext();



            // Add the question without specifying the ID
            db.Questions.Add(question);
            db.SaveChanges();

            // Get the ID of the added question
            int addedQuestionId = question.QuestionId;

            // Now associate the choices with the question using the retrieved ID

            for (int i = 0; i < c.Length; i++)
            {



                var choice = new Choice
                {
                    QuestionId = addedQuestionId,
                    Content = c[i],
                    // IsCorrect = content[i] == answer

                    IsCorrect = (answer == "1" || answer == "2" || answer == "3" || answer == "4")
                    && answer == (i + 1).ToString()


                };
                db.Choices.Add(choice);

            }
            db.SaveChanges();
            return RedirectToAction("List");
        }



        [HttpPost]
        public async Task<IActionResult> DeleteQuestion(int id)
        {
            PRN211_BL5DBContext db = new PRN211_BL5DBContext();

            if (id <= 0)
            {
                return NotFound();
            }

            var question = await db.Questions
                .Include(q => q.Choices)
                .FirstOrDefaultAsync(q => q.QuestionId == id);

            if (question == null)
            {
                return NotFound();
            }

            db.Choices.RemoveRange(question.Choices);
            db.Questions.Remove(question);

            await db.SaveChangesAsync();

            TempData["msg"] = $"Deleted question with ID: {id}";
            return RedirectToAction("List");
        }



    }
}
