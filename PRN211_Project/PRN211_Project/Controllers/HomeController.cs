﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PRN211_Project.Models;
using System.Diagnostics;
using System.Text.Json;
using System.Text.Json.Serialization;
using PRN211_Project.Logics;
using Microsoft.EntityFrameworkCore;

namespace PRN211_Project.Controllers
{
    public class HomeController : Controller
    {
        UserManager user = new UserManager();
        private readonly ILogger<HomeController> _logger;
        private PRN211_BL5DBContext db = new PRN211_BL5DBContext();


        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }
        public IActionResult HomePage()
        {

            ViewBag.User = HttpContext.Session.GetInt32("UserId");
            ViewBag.Role = HttpContext.Session.GetInt32("UserRole");
            return View();
        }
        public async Task<IActionResult> LogOut()
        {
            //HttpContext.Session.Remove("user");
            HttpContext.Session.Remove("UserId");
            HttpContext.Session.Remove("UserRole");
            HttpContext.Session.Remove("avatar");
            return RedirectToAction("HomePage", "Home");
        }
        public IActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Login_submit(string userName, string password)
        {
            var user = await db.UserRoles.Include(x => x.Role).Include(x => x.User).FirstOrDefaultAsync(x => x.User.Username == userName && x.User.Password == password);
            if (user == null)
            {
                TempData["msg"] = "User email or password is not correct. Please check again!";
                return View("Login");
            }
            if (user.IsActive == 0)
            {
                TempData["msg"] = "This account has been banned. Please contact us via email for more infomation!";
                return View("Login");
            }

            //TempData["msg"] = $"{user.Role.RoleName}";
            User u = user.User;
            //HttpContext.Session.SetString("user", JsonSerializer.Serialize(u));
            HttpContext.Session.SetInt32("UserId", user.User.UserId);
            //HttpContext.Session.SetString("UserName", user.User.Username);
            HttpContext.Session.SetInt32("UserRole", user.RoleId);
            HttpContext.Session.SetString("avatar", user.User.Avatar);


            return RedirectToAction("HomePage", "Home");

        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> Register()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Register(User u, string repass)
        {
            if (!ModelState.IsValid)
            {
                return View("Register");
            }
            else if (!u.Password.Equals(repass))
            {
                TempData["errorpass"] = "The re-password is not correct!";
                return View("Register");
            }

            //Console.WriteLine("User:" + u.UserId);
            db.Users.Add(u);
            db.SaveChanges();

            var temp1 = await db.Users.OrderBy(x => x.UserId).LastAsync();
            //u.UserId = temp1.UserId + 1;
            db.UserRoles.Add(new UserRole()
            {
                UserId = temp1.UserId,
                RoleId = 2,
                IsActive = 1
            });
            db.SaveChanges();
            return RedirectToAction("Login");
        }

    }
}