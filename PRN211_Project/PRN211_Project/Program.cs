﻿using Microsoft.EntityFrameworkCore;
using PRN211_Project.Models;
using PRN211_Project.Utility;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddControllersWithViews(); //Web hoat dong tren co so tu controller sang View
//builder.Services.AddSession();
// Add services to the container.
builder.Services.AddDistributedMemoryCache();
builder.Services.AddSession(options =>
{
    options.IdleTimeout = TimeSpan.FromMinutes(60);
    options.Cookie.HttpOnly = true;
    options.Cookie.IsEssential = true;
});
builder.Services.AddOptions();                                         // Kích hoạt Options
var mailsettings = builder.Configuration.GetSection("MailSettings");  // đọc config
builder.Services.Configure<MailSettings>(mailsettings);                // đăng ký để Inject
//bổ sung service cho ứng dụng vận hành theo kiến trúc MVC
// Đăng ký SendMailService với kiểu Transient, mỗi lần gọi dịch
// vụ ISendMailService một đới tượng SendMailService tạo ra (đã inject config)
builder.Services.AddTransient<ISendMailService, SendMailService>();

//bo sung instance cua PRN211DBcontext vao container cua web server
builder.Services.AddDbContext<PRN211_BL5DBContext>(opt => opt.UseSqlServer(builder.Configuration.GetConnectionString("Value")));
var app = builder.Build();
app.UseSession();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=HomePage}"
    );
app.UseStaticFiles();
app.Run();
